//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Harry Gardiner on 02/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#pragma once

#include <stdio.h>

class MidiMessage
{
public:
    MidiMessage();
    
    MidiMessage(int Inumber, int Ivelocity, int Ichannel);
    
    ~MidiMessage();
    
    void setNoteNumber(int newNoteNumber);
    
    int getNoteNumber() const;
    
    float getMidiNoteInHertz() const;
    
    void setVelocity(int newVelocity);
    
    int getVelocity() const;
    
    void setChannel(int newChannel);
    
    int getChannel() const;
    
private:
    int noteNumber;
    int velocity;
    int channel;
};


