//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Harry Gardiner on 02/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.hpp"
#include <iostream>
#include <cmath>

MidiMessage::MidiMessage()
{
    noteNumber = 60;
    velocity = 0;
    channel = 1;
    std::cout << "Constructed\n";
}
MidiMessage::MidiMessage(int Inumber, int Ivelocity, int Ichannel)
{
    noteNumber = Inumber;
    velocity = Ivelocity;
    channel = Ichannel;
    std::cout << "Constructed\n";
}
MidiMessage::~MidiMessage()
{
    std::cout << "\nEnded\n";
}
void MidiMessage::setNoteNumber(int newNoteNumber)
{
    noteNumber = newNoteNumber;
}
int MidiMessage::getNoteNumber() const
{
    return noteNumber;
}
float MidiMessage::getMidiNoteInHertz() const
{
    return 440 * pow(2, (noteNumber-69) / 12.0);
}

int MidiMessage::getChannel() const
{
    return channel;
}
int MidiMessage::getVelocity() const
{
    return velocity;
}
