//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include <cmath>
#include "MidiMessage.hpp"


int main()
{
    int number;
    int velocity;
    int channel;
    
    std::cout << "Insert note number ";
    std::cin >> number;
    std::cout << "Insert velocity ";
    std::cin >> velocity;
    std::cout << "Insert channel ";
    std::cin >> channel;
    
    if (number>=0&&number<=127&&velocity>=0&&velocity<=127&&channel>=0&&channel<=16) {
        MidiMessage note(number, velocity, channel);
        std::cout << "\nNote Number = " << note.getNoteNumber();
        std::cout << "\nFrequency = " << note.getMidiNoteInHertz();
        std::cout << "\nVelocity = " << note.getVelocity();
        std::cout << "\nChannel = " << note.getChannel();
        
    }
    else{
        std::cout << "Invalid values\n";
        
    }
    return 0;
    
}


